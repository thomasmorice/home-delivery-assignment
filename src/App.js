import React from 'react';
import './App.scss';
import DeliveryTime from './DeliveryTime/DeliveryTime'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deliveryTimes: [],
      deliveryTimeSelected: false,
      deliveryAtHome: false,
    };
  }
  
  componentDidMount() {
    fetch('/delivery-times.json')
      .then(response => response.json())
      .then((deliveryTimes) => {
        // Order the object by starting time
        // Todo: clean the data, order by id so we can remove this
        deliveryTimes.sort((a,b) => (
          a.startTime.substring(0, 2) > b.startTime.substring(0, 2)) ? 1 : 
          (b.startTime.substring(0, 2) > a.startTime.substring(0, 2)) ? -1 : 0
        )

        deliveryTimes.map((deliveryTime) => {
            let startTimeFormatted = deliveryTime.startTime.substring(0, deliveryTime.startTime.length - 3)
            let endTimeFormatted = deliveryTime.stopTime.substring(0, deliveryTime.stopTime.length - 3)
            deliveryTime.startStopTime =  startTimeFormatted + ' - ' + endTimeFormatted

          return deliveryTime
        })
        this.setState({ deliveryTimes })
      })
      .catch(err => {
        console.error('Error while fetching the json file')
        console.error(err)
      });
  }

  handleDeliveryAtHomeCheckbox = () => {
    this.setState({
      deliveryAtHome: !this.state.deliveryAtHome,
      deliveryTimeSelected: false
    })
  }

  onDeliveryTimeSelected = (id) => {
    this.setState({
      deliveryTimeSelected: id,
    })
  }

  deliveryTimes = () => {
    return (
      this.state.deliveryTimes.map((deliveryTime) => {
        if ((this.state.deliveryAtHome && deliveryTime.inHomeAvailable) || !this.state.deliveryAtHome) {
          return (
            <div 
              onClick={() => this.onDeliveryTimeSelected(deliveryTime.deliveryTimeId)} 
              key={deliveryTime.deliveryTimeId}
            >
              <DeliveryTime
                deliveryTime={deliveryTime}
                idSelected={this.state.deliveryTimeSelected}
              />
            </div>
          )
        } else {
          return null
        }
      })
    );
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
            <h1>Home Delivery Assignment</h1>
            <h2> The assignment</h2>
            <p>
              Create a web view showing delivery time slots based on provided JSON-data, HTML and CSS.
            </p>
        </header>
        <div className="delivery-container"> 
          <h3>Choose your delivery time</h3>
          <input id="checkbox-home-delivery" 
            type="checkbox" 
            checked={this.state.deliveryAtHome}
            onChange={this.handleDeliveryAtHomeCheckbox}
            className="checkbox-home-delivery"/>
          <label htmlFor="checkbox-home-delivery" className="label-home-delivery">I want to be delivered at home</label>
          <div className="delivery-times-container"> 
            { this.deliveryTimes() }
          </div>
        </div>
        <div>
          <h3>Payment</h3>
          <div className="validate-order"> 
            <button className={!this.state.deliveryTimeSelected ? 'disabled' : ''} disabled={!this.state.deliveryTimeSelected}> Next - Payment method </button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
