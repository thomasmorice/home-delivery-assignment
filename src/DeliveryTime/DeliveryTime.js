import React from 'react';
import './DeliveryTime.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deliveryTime: props.deliveryTime
    };
  }

  render() {
    return (
      <div className={`Delivery-time ${this.props.idSelected === this.state.deliveryTime.deliveryTimeId ? 'selected' : ''}`}>
        <div className="delivery-hours">{this.state.deliveryTime.startStopTime}</div>
        <div>{this.state.deliveryTime.inHomeAvailable ? '✔ home delivery' : '❌ home delivery'}</div>
      </div>
    );
  }
}

export default App;
